// Select all buttons with the "tag" class
const buttons = document.querySelectorAll('.tag');

// Define a function to randomly replace a button's class with "btn-primary" for 1 second
function highlightRandomButton() {
  // Select a random button from the list of buttons
  const randomButton = buttons[Math.floor(Math.random() * buttons.length)];
  // Replace the "btn-outline-secondary" class with "btn-primary" on the selected button
  randomButton.classList.replace('btn-outline-success', 'btn-success');
  // Remove the "btn-primary" class after 1 second
  setTimeout(() => {
    randomButton.classList.replace('btn-success', 'btn-outline-success');
  }, 1000);
}

// Call the "highlightRandomButton" function every 1 second
setInterval(highlightRandomButton, 1000);

// ----------------------------------
const flip = document.querySelector('.flip');

flip.addEventListener('click', function() {
  flip.classList.toggle('rotate');
});

// ----------------------------------
// Récupérez les éléments des images recto et verso
var recto = document.querySelector('.recto');
var verso = document.querySelector('.verso');

// Écoutez les clics sur l'image recto
recto.addEventListener('click', function() {
  // Masquez l'image recto et affichez l'image verso
  recto.classList.add('d-none');
  verso.classList.remove('d-none');
});

// Écoutez les clics sur l'image verso
verso.addEventListener('click', function() {
  // Masquez l'image verso et affichez l'image recto
  verso.classList.add('d-none');
  recto.classList.remove('d-none');
});

// ----------------------------------
const modal = document.querySelector(".modal");
const openBtn = document.querySelector(".open-btn");
const closeBtn = document.querySelector(".close-btn");

console.log('new modal ok');

openBtn.addEventListener("click", () => {
  modal.style.display = "flex";
  console.log('open');
});

closeBtn.addEventListener("click", () => {
  modal.querySelector(".modal-content").style.animation = "slide-up 0.5s forwards";
  setTimeout(() => {
    modal.style.display = "none";
    modal.querySelector(".modal-content").style.animation = "";
  }, 500);
});

// ----------------------------------




// // Set the initial position of each slide
// slides.forEach((slide, index) => {
//   slide.style.left = `${index * 100}%`;
// });

// // Function to move the slider to the next slide
// const nextSlide = () => {
//   currentIndex++;

//   if (currentIndex > slides.length - 1) {
//     currentIndex = 0;
//   }

//   slider.style.transform = `translateX(-${currentIndex * 100}%)`;
// };

// // Function to move the slider to the previous slide
// const prevSlide = () => {
//   currentIndex--;

//   if (currentIndex < 0) {
//     currentIndex = slides.length - 1;
//   }

//   slider.style.transform = `translateX(-${currentIndex * 100}%)`;
// };

// // Event listeners for the previous and next buttons
// prevBtn.addEventListener("click", prevSlide);
// nextBtn.addEventListener("click", nextSlide);

// // Automatically move to the next slide every 3 seconds
// setInterval(nextSlide, 3000);

